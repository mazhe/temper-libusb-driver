#include <arpa/inet.h> /* For ntohs() */
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libusb.h>

#define CHECKCALL(call) {int r_ = call; if (r_ < 0) {fprintf(stderr, "%s:%d:%s: %s\n", __FILE__, __LINE__, #call, libusb_strerror(r_)); goto error;}}

bool
is_temper(uint16_t vendor_id, uint16_t product_id)
{
	const uint16_t TEMPER_IDS[][2] = {
		{0x1a86, 0xe025},
		{0x3553, 0xa001}
	};
	const size_t TEMPER_IDS_LEN = sizeof(TEMPER_IDS)/(2*sizeof(uint16_t));

	for (size_t i = 0; i < TEMPER_IDS_LEN; i++) {
		if (vendor_id == TEMPER_IDS[i][0] && product_id == TEMPER_IDS[i][1]) {
			return true;
		}
	}
	return false;
}

float
read_temperature(libusb_device_handle *devh)
{
	struct libusb_config_descriptor *config_desc;
	uint8_t getT[8] = {0x01, 0x80, 0x33, 0x01, 0x00, 0x00, 0x00, 0x00 };
	int getT_transferred;
	uint8_t resp[8];
	int resp_transferred;

	CHECKCALL(libusb_get_active_config_descriptor(libusb_get_device(devh), &config_desc));
	if (libusb_kernel_driver_active(devh, 0) || libusb_kernel_driver_active(devh, 1)) {
		CHECKCALL(libusb_set_auto_detach_kernel_driver(devh, 1));
	}
	CHECKCALL(libusb_claim_interface(devh, 0));
	CHECKCALL(libusb_claim_interface(devh, 1));

	CHECKCALL(libusb_interrupt_transfer(devh, 0x02, getT, sizeof(getT), &getT_transferred, 1000));
	CHECKCALL(libusb_interrupt_transfer(devh, 0x82, resp, sizeof(resp), &resp_transferred, 1000));

	libusb_release_interface(devh, 0);
	libusb_release_interface(devh, 1);

	return ntohs(*((int16_t *)(resp+2)))/100.;

error:
	libusb_release_interface(devh, 0);
	libusb_release_interface(devh, 1);
	return NAN;
}

int
main(int argc, char *argv[])
{
	int ch;
	float offset = 0.;
	libusb_context *ctx = NULL;
	libusb_device **list = NULL;
	ssize_t listlen;

	while ((ch = getopt(argc, argv, "O:")) != -1) {
		switch (ch) {
		case 'O':
			offset = strtod(optarg, NULL);
			break;
		case '?':
		default:
			exit(EXIT_FAILURE);
		}
	}

	CHECKCALL(libusb_init(&ctx));

	listlen = libusb_get_device_list(ctx, &list);
	for (ssize_t i = 0; i < listlen; i++) {
		struct libusb_device_descriptor desc;
		libusb_device_handle *devh;
		float temperature;

		CHECKCALL(libusb_get_device_descriptor(list[i], &desc));
		if (!is_temper(desc.idVendor, desc.idProduct)) {
			continue;
		}

		CHECKCALL(libusb_open(list[i], &devh));
		temperature = read_temperature(devh);
		if (isfinite(temperature)) {
			printf("%.2f\n", (temperature+offset));
		}
		libusb_close(devh);
	}
	libusb_free_device_list(list, 1);
	libusb_exit(ctx);

	return EXIT_SUCCESS;

error:
	if (list != NULL) {
		libusb_free_device_list(list, 1);
	}
	if (ctx != NULL) {
		libusb_exit(ctx);
	}
	return EXIT_FAILURE;
}
