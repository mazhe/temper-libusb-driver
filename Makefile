
CFLAGS+=-g -Wall -Wextra -Werror
LIBUSB?=-lusb

CONFIG?=null
include mk/$(CONFIG).inc.mk

temper: temper.o
	$(CC) $(LDFLAGS) -o $@ temper.o $(LIBUSB) -lm

.PHONY: clean
clean:
	rm -f temper *.o
