# temper-driver

A libusb (1.0) program to read temperature from USB TEMPer thermometers.

## Building

libusb headers and library won't probably be directly in defaults path or names can change.

Either reuse or create a config file in the mk/ subdirectory and adjust CFLAGS, LDFLAGS, LIBUSB variables as needed. There is already some config available.

Then:

```
$ make CONFIG=myconfig
```

## Device permissions for regular users

### FreeBSD

Ensute your devfs rules give USB permission to the users, for ex in `/etc/devfs.rules`:
```
[localrules=10]
add path 'usb/*' mode 0660 group operator
```
And restart devfs:
```
# service devfs restart
```

Edit `/usr/local/etc/devd/temper.conf`:
```
notify 100 {
    match "system"      "USB";
    match "subsystem"   "DEVICE";
    match "type"        "ATTACH";
    match "vendor"      "0x1a86";
    match "product"     "0xe025";
	action "usbconfig -i 0 detach_kernel_driver $cdev; usbconfig -i 1 detach_kernel_driver $cdev;";
};
```
Then restart devd:
```
# service devd restart
```

### GNU/Linux (udev)

Edit `/etc/udev/rules.d/50-temper.rules`:
```
SUBSYSTEM=="usb", ATTR{idVendor}=="1a86", ATTR{idProduct}=="e025", MODE="0666"
```
Then update rules:
```
# udevadm control --reload-rules
```
